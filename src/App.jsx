import React, { useEffect, useState } from "react";

import ClipLoader from "react-spinners/ClipLoader";

import { useDarkMode } from "./Context/DarkModeContext.jsx";

import Header from "./Components/Header.jsx";
import Country from "./Components/Country.jsx";
import Search from "./Components/Search.jsx";
import Filter from "./Components/Filter.jsx";
import Sort from "./Components/Sort.jsx";
import ErrorPage from "./Components/ErrorPage.jsx";

import "./App.css";

function App() {
  const { darkMode, styles } = useDarkMode();
  const [countries, setCountries] = useState([]);
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);
  const [sort, setSort] = useState("");
  const [error, setError] = useState(false);

  const totalRegionArray = countries.reduce((acc, country) => {
    if (!acc[country.region]) {
      acc[country.region] = [];
    }
    if (country.subregion && !acc[country.region].includes(country.subregion)) {
      acc[country.region].push(country.subregion);
    }

    return acc;
  }, {});

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");
        const result = await response.json();
        setCountries(result);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setError(true);
        console.error(error.message);
      }
    };

    fetchData();
  }, []);

  const handleRegionChange = (newRegion) => {
    setRegion(newRegion);
    setSubRegion("");
  };
  const handleSubRegionChange = (newRegion) => {
    setSubRegion(newRegion);
  };

  return (
    <div>
      <Header />
      <div
        className="searchAndFilter"
        style={darkMode ? styles.dark : styles.light}
      >
        <Search setSearch={setSearch} />
        <Sort setSort={setSort} />
        <Filter
          type="region"
          totalRegionArray={Object.keys(totalRegionArray)}
          setRegion={handleRegionChange}
        />
        <Filter
          type="subregion"
          totalRegionArray={region ? totalRegionArray[region] : []}
          setRegion={handleSubRegionChange}
        />
      </div>
      {error && <ErrorPage />}
      {loading && (
        <div className="error-page">
          <ClipLoader
            size={100}
            aria-label="Loading Spinner"
            data-testid="loader"
          />
        </div>
      )}
      {!loading && !error && (
        <Country
          countries={countries}
          region={region}
          subRegion={subRegion}
          search={search}
          sort={sort}
        />
      )}
    </div>
  );
}

export default App;
