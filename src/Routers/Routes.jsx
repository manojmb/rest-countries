import React from "react";

import { createBrowserRouter } from "react-router-dom";

import App from "../App.jsx";
import ErrorPage from "../Components/ErrorPage.jsx";
import CountryDetail from "../Components/CountryDetail.jsx";

const routes = [
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/country/:id",
    element: <CountryDetail />,
    errorElement: <ErrorPage />,
  },
];

const router = createBrowserRouter(routes);

export default router;
