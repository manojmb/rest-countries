import React, { createContext, useContext, useState } from "react";

const DarkModeContext = createContext();

export const DarkModeProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);

  const styles = {
    light: {
      backgroundColor: "var(--Very-Light-Gray)",
      color: "var(--Very-Dark-Blue)",
    },
    dark: {
      backgroundColor: "var(--Very-Dark-Blue-dark)",
      color: "var(--White)",
    },
    elementsLight: {
      backgroundColor: "var(--White)",
      color: "var(--Very-Dark-Blue)",
    },
    elementsDark: {
      backgroundColor: "var(--Dark-Blue)",
      color: "var(--White)",
    },
    inputLight: {
      backgroundColor: "var(--White)",
      color: "var(--Dark-Gray)",
    },
  };

  const toggleDarkMode = () => {
    setDarkMode((prevMode) => !prevMode);
  };

  return (
    <DarkModeContext.Provider value={{ darkMode, toggleDarkMode, styles }}>
      {children}
    </DarkModeContext.Provider>
  );
};

export const useDarkMode = () => {
  const context = useContext(DarkModeContext);

  return context;
};
