import { useRouteError } from "react-router-dom";

function ErrorPage() {
  const error = useRouteError();

  return (
    <>
      <div class="error-page">
        <h1>Oops!</h1>
        <p>
          Sorry, an unexpected error has occurred. Please provide proper route
          or refresh
        </p>
        <p>
          <i>{error.statusText || error.message}</i>
        </p>
      </div>
    </>
  );
}

export default ErrorPage;
