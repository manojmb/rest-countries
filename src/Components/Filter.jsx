import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";

import { useDarkMode } from "../Context/DarkModeContext";

function Filter({ totalRegionArray, setRegion, type }) {
  const { darkMode, styles } = useDarkMode();

  return (
    <div
      className="filter"
      style={darkMode ? styles.elementsDark : styles.elementsLight}
    >
      <FontAwesomeIcon icon={faFilter} />
      <select
        name="region"
        onChange={(event) => setRegion(event.target.value)}
        style={darkMode ? styles.elementsDark : styles.elementsLight}
        defaultValue="default"
      >
        <option value="default" disabled hidden>
          Filter by {type}
        </option>
        <option value="">All</option>
        {totalRegionArray.map((region) => {
          return (
            <option key={region} value={region}>
              {region}
            </option>
          );
        })}
      </select>
    </div>
  );
}

export default Filter;
