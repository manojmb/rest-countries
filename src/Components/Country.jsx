import { Link } from "react-router-dom";

import { useDarkMode } from "../Context/DarkModeContext";

function Country({ countries, region, subRegion, search, sort }) {
  const { darkMode, styles } = useDarkMode();
  let filteredCountries = [];
  if (countries.length != 0) {
    filteredCountries = countries.filter(
      (country) =>
        (!region || country.region === region) &&
        (!subRegion || country.subregion === subRegion) &&
        (!search ||
          country.name.common.toLowerCase().includes(search.toLowerCase()))
    );

    if (sort) {
      let category = sort.split("-")[0];
      filteredCountries.sort(
        (countryA, countryB) => countryA[category] - countryB[category]
      );

      if (sort.split("-")[1] === "desc") {
        filteredCountries.reverse();
      }
    }
  }

  return (
    <main style={darkMode ? styles.dark : styles.light}>
      <div className="container" style={darkMode ? styles.dark : styles.light}>
        {filteredCountries.length > 0 ? (
          filteredCountries.map((country) => (
            <div
              className="flagContainer"
              key={country.cca3}
              style={darkMode ? styles.elementsDark : styles.elementsLight}
            >
              <Link to={`country/${country.cca3}`}>
                <div className="flag">
                  <img src={country.flags.png} alt="" />
                </div>
                <div className="countryDetails">
                  <h3 className="countryName">{country.name.common}</h3>
                  <p className="population">Population: {country.population}</p>
                  <p className="region">Region: {country.region}</p>
                  <p className="area">Area: {country.area}</p>

                  <p className="capital">
                    Capital: {country.capital || "no capital"}
                  </p>
                </div>
              </Link>
            </div>
          ))
        ) : (
          <div className="error-page">No such Country</div>
        )}
      </div>
    </main>
  );
}

export default Country;
