import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

import { useDarkMode } from "../Context/DarkModeContext";

function Search({ setSearch }) {
  const { darkMode, styles } = useDarkMode();

  const handleSearchCountry = (inputValue) => {
    setSearch(inputValue);
  };

  return (
    <div
      className="searchBar"
      style={darkMode ? styles.elementsDark : styles.inputLight}
    >
      <FontAwesomeIcon icon={faMagnifyingGlass} />
      <input
        type="text"
        placeholder="Search for a country ..."
        onChange={(event) => {
          handleSearchCountry(event.target.value);
        }}
        style={darkMode ? styles.elementsDark : styles.inputLight}
      />
    </div>
  );
}

export default Search;
