import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

import ClipLoader from "react-spinners/ClipLoader";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeftLong } from "@fortawesome/free-solid-svg-icons";

import { useDarkMode } from "../Context/DarkModeContext.jsx";

import Header from "./Header";
import ErrorPage from "./ErrorPage.jsx";

const CountryDetail = () => {
  const { darkMode, styles } = useDarkMode();
  const { id } = useParams();
  const [countryData, setCountryData] = useState(null);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchCountry = async () => {
      try {
        const response = await fetch(
          `https://restcountries.com/v3.1/alpha/${id}`
        );
        const result = await response.json();
        if (result.status === 400) {
          setError(true);
        }
        setCountryData(result[0]);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setError(true);
      }
    };

    fetchCountry();
  }, [id]);

  return (
    <>
      <Header />
      <div className="flagDetail" style={darkMode ? styles.dark : styles.light}>
        <Link to={"/"}>
          <div className="backBtnContainer">
            <button
              className="backBtn"
              style={darkMode ? styles.elementsDark : styles.elementsLight}
            >
              <FontAwesomeIcon icon={faArrowLeftLong} />
              Back
            </button>
          </div>
        </Link>
        {error ? <ErrorPage /> : null}
        {loading ? (
          <div className="error-page">
            <ClipLoader
              size={100}
              aria-label="Loading Spinner"
              data-testid="loader"
            />
          </div>
        ) : null}
        {!error && !loading && countryData ? (
          <div className="flagDetailContainer">
            <img src={countryData.flags.svg} alt="country flag" />
            <div className="details">
              <h2>{countryData.name.common}</h2>
              <div className="countryStats">
                <div>
                  <p>
                    <strong>Native Name:</strong>
                    {countryData.name.nativeName
                      ? countryData.name.nativeName.eng
                        ? countryData.name.nativeName.eng.common
                        : Object.values(countryData.name.nativeName).slice(
                            -1
                          )[0].common
                      : "no native name"}
                  </p>
                  <p>
                    <strong>Population:</strong>
                    {countryData.population}
                  </p>
                  <p>
                    <strong>Region:</strong>
                    {countryData.region}
                  </p>
                  <p>
                    <strong>Sub Region:</strong>
                    {countryData.subregion
                      ? countryData.subregion
                      : "no subregion"}
                  </p>
                  <p>
                    <strong>Capital:</strong>
                    {countryData.capital ? countryData.capital : "no capital"}
                  </p>
                </div>

                <div>
                  <p>
                    <strong>Top Level Domain:</strong>
                    {countryData.tld ? countryData.tld[0] : "no domain"}
                  </p>
                  <p>
                    <strong>Currencies:</strong>
                    {countryData.currencies
                      ? Object.values(countryData.currencies)[0].name
                      : "No currency"}
                  </p>
                  <p>
                    <strong>Languages:</strong>
                    {countryData.languages
                      ? Object.values(countryData.languages).join(", ")
                      : "no language"}
                  </p>
                </div>
              </div>
              <div className="borderCountries">
                <strong>Border Countries:</strong>
                {countryData.borders ? (
                  countryData.borders.map((border) => {
                    return (
                      <Link key={border} to={`/country/${border}`}>
                        <button
                          style={
                            darkMode
                              ? styles.elementsDark
                              : styles.elementsLight
                          }
                        >
                          {border}
                        </button>
                      </Link>
                    );
                  })
                ) : (
                  <p>No border</p>
                )}
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default CountryDetail;
