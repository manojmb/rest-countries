import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSort } from "@fortawesome/free-solid-svg-icons";

import { useDarkMode } from "../Context/DarkModeContext";

function Sort({ setSort }) {
  const { darkMode, styles } = useDarkMode();

  const handleSortCountry = (inputValue) => {
    setSort(inputValue);
  };

  return (
    <div
      className="sort"
      style={darkMode ? styles.elementsDark : styles.elementsLight}
    >
      <FontAwesomeIcon icon={faSort} />
      <select
        name="order"
        onChange={(event) => handleSortCountry(event.target.value)}
        style={darkMode ? styles.elementsDark : styles.elementsLight}
        defaultValue=""
      >
        <option value="">Sort by</option>
        <optgroup label="Population">
          <option value="population-asc">Ascending</option>
          <option value="population-desc">Descending</option>
        </optgroup>
        <optgroup label="Area">
          <option value="area-asc">Ascending</option>
          <option value="area-desc">Descending</option>
        </optgroup>
      </select>
    </div>
  );
}

export default Sort;
