import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";

import { useDarkMode } from "../Context/DarkModeContext";

function Header() {
  const { darkMode, toggleDarkMode, styles } = useDarkMode();

  return (
    <div
      className="header"
      style={darkMode ? styles.elementsDark : styles.elementsLight}
    >
      <h4>Where in the world?</h4>
      <button className="darkMode" onClick={toggleDarkMode}>
        <FontAwesomeIcon icon={faMoon} />
        Dark Mode
      </button>
    </div>
  );
}

export default Header;
